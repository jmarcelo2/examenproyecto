<?php
/** @var yii\web\View $this */
use yii\grid\GridView;
$this->title = 'My Yii Application';
?>
<div class="site-index">
    <h1 style="text-align: center">Mi asombroso examen 1-1</h1>
    
    <div>
         <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nompuerto',
            'dorsal',
        ],
    ]); ?>
    </div>
</div>
