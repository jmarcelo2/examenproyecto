<?php

namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Puerto;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
     public function actionConsulta1()
    {
        
       //mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("nompuerto,dorsal")->distinct(),
        ]);
        
        return $this->render("index",[
            "resultados"=>$dataProvider,
            "campos"=>["nompuerto,dorsal"],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar el puerto y dorsal de los ciclistas con altura mayor",
            "sql"=>"SELECT DISTINCT nompuerto, dorsal FROM puerto where altura=1000"
        ]);
    }
    
        public function actionConsulta2()
    {
        
       //mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()->select("numpuerto,dorsal")->distinct(),
        ]);
        
        return $this->render("index",[
            "resultados"=>$dataProvider,
            "campos"=>["numetapa,kms"],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Listar etapas",
            "sql"=>"SELECT DISTINCT numetapa, kms from etapa where llegada!=salida"
        ]);
    }
    
        public function actionConsulta3()
    {
        
       //mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("nompuerto,dorsal")->distinct(),
        ]);
        
        return $this->render("index",[
            "resultados"=>$dataProvider,
            "campos"=>["nompuerto,dorsal"],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Listar el puerto y dorsal de los ciclistas con altura mayor de 1500",
            "sql"=>"SELECT DISTINCT nompuerto, dorsal FROM puerto where altura=1500"
        ]);
    }
}
